# Energy @ Home

This project aims to help me evaluate a solar power system (PV-plant) with
energy storage for my home. My main questions are:

- Should I invest into a PV-plant?
    - How big should the plant be?
    - Which modules should I use?
    - What components will I need?
- Should I invest into an energy storage (battery)?
    - How big should the battery be?
    - How should the battery operate?
- What should we do about the heatpump?
    - Should we keep it attached to our power meter to save money from
      electricity costs?
    - Is it better to attach it to a separate power meter with a cheap
      contract?

These questions are dependent on each other, of course. The "optimal" size of a
battery will depend on the size of the PV and the load profile of all other
devices in the household. Finding a definition for "optimal" is also a very
subjective task. One can optimize, for example, the amortization time, the
self-reliance of the entire system, or the money gained per produced unit of
energy.

I don't know yet where I want to go, that's why I'm starting this project.


## Components

The full system will consist of multiple components. For the sake of analysis,
I'm dividing them here by their individual load profiles:

1. *Regular household energy consumption in my apartment*: This includes
   cooking, laundry and everyday-electric devices (TV, PC)
2. *Wallbox for EV-charging*: I have an EV and a Wallbox at the stall
3. *Heatpump for general heating*: The heatpump is powered by electricity, too,
   and supplies heat to my appartment and our neighbors as well
4. *Solar power system*: This is the only energy production I'll have
5. *Energy storage*: While not exactly a load profile on its' own, it
   influences all the other load profiles.


## Research Notes

Evaluating this kind of system requires data about expected load profiles. With
load profiles, I can perform calculations and estimates about e.g. energy
storage capacity levels per day/hour of the day, production cost per energy
unit and amortization time.

In Germany, there are so-called "Default load profiles" which are normed load
profiles used by e.g. electricity distributors to coordinate energy production
capacity. These were initially distributed by the VDEW (Verband der
Elektrizitätswirtschaft e.V.) in cooperation with the University Cottbus. In
2007 the VDEW has become part of the [BDEW][bdew] (Bundesverband der Energie-
und Wasserwirtschaft) which is distributing these load profiles today [on their
website][bdew_load_profiles].

A textual overview of all the different types of load profiles the electricity
providers distinguish can be found here:
https://www.bayernwerk-netz.de/de/energie-anschliessen/netznutzung-strom/lastprofilverfahren.html



[bdew]: https://www.bdew.de
[bdew_load_profiles]: https://www.bdew.de/energie/standardlastprofile-strom/
